import Vue from 'vue';
import App from './App.vue';
import vuetify from './plugins/vuetify';
import store from './plugins/vuex';
import router from './plugins/vue-router';
import AnimateCSS from 'animate.css';
import Toasted from 'vue-toasted';
import HighchartsVue from 'highcharts-vue';
import Highcharts from 'highcharts';
import item from 'highcharts/modules/item-series';
import bullet from 'highcharts/modules/bullet';


Vue.use(AnimateCSS);

// Highcharts modules
item(Highcharts);
bullet(Highcharts);
Vue.use(HighchartsVue);

const toastOptions = {
  //theme: 'bubble',
  position: "bottom-center",
};
Vue.use(Toasted, toastOptions);

Vue.config.productionTip = false;

new Vue({
  vuetify,
  store,
  router,
  render: h => h(App)
}).$mount('#app');
