const state = () => ({
  favourites: ["9484dbf0-fa04-4f81-8646-5f16a6f58860", "fc3a1e0a-70b3-4d91-b1d0-56ee91559ed0"], // this should load from DB. Default is CFT
  alwaysFavs: false,
  darkMode: false
});

const getters = {
  isFavourite: (state) => (id) => {
    return state.favourites.includes(id);
  }
};

const mutations = {
  toggleFavourite(state, id) {
    // if id exists in favourites, remove it, else add it
    state.favourites = state.favourites.includes(id) ?
      state.favourites.filter(f => f != id) :
      [...state.favourites, id];
  },
  setDarkMode(state, val) {
    state.darkMode = val;
  },
  setAlwaysFavs(state, val) {
    state.alwaysFavs = val;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  //actions,
  mutations
};