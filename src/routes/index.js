export default [
  {
    path: "/",
    redirect: "/dashboard"
  },
  {
    path: "/dashboard",
    component: require('/src/pages/TheDashboard').default,
    children: [
      {
        path: "",
        component: require('/src/components/DashboardView').default,
        props: {
          dashboardSections: ['Favourites', 'Apps', 'Insights']
        }
      },
      {
        path: "favourites",
        component: require('/src/components/DashboardView').default,
        props: {
          dashboardSections: ['Favourites']
        }
      },
      {
        path: "insights",
        component: require('/src/components/DashboardView').default,
        props: {
          dashboardSections: ['Insights']
        },
      },
      {
        path: "insights/jlu-risks",
        component: require('/src/components/DashboardView').default,
        props: {
          dashboardSections: ['SCB JLU Risks']
        },
      },
      {
        path: "insights/pump",
        component: require('/src/components/DashboardView').default,
        props: {
          dashboardSections: ['SCB PuMP']
        },
      },
      {
        path: "apps",
        component: require('/src/components/DashboardView').default,
        props: {
          dashboardSections: ['Favourites', 'Apps']
        }
      },
    ]
  },

];
