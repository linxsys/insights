export default {
  chart: {
    spacing: [0, 0, 0, 0],
    margin: [0, 0, 0, 0],
    height: "75px",
    backgroundColor: "transparent",
  },
  credits: {
    enabled: false
  },
  title: {
    text: null,
  },
  tooltip: {
    hideDelay: 100,
  },
  yAxis: {
    visible: false,
  },
  legend: {
    enabled: false,
  },
};