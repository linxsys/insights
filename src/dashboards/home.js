import favs from "./favourites.ts";
import apps from "./apps";
import insights from "./insights";

export default [...favs, ...insights, ...apps];
