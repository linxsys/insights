export default [
  {
    "section": "Apps",
    "tiles": [
      {
        "id": "9484dbf0-fa04-4f81-8646-5f16a6f58860",
        "title": "CFT",
        "description": "SCB Customer Feedback Tool",
        "contentComponent": require('/src/components/IconImg').default,
        "contentProps": {
          "img": "cft_l.png"
        }
      },
      {
        "id": "cd0ae55e-dec2-4592-a77a-4f629f4ac1d6",
        "title": "ELMS",
        "description": "Enhanced Loans Management System",
        "contentComponent": require('/src/components/IconImg').default,
        "contentProps": {
          "img": "elms_l.png"
        }
      },
      {
        "id": "78596f8a-b50c-4281-991b-840768c28f77",
        "title": "WATS",
        "description": "Warehouse Activity Tracking System",
        "contentComponent": require('/src/components/IconImg').default,
        "contentProps": {
          "img": "wats_l.png"
        }
      },
      {
        "id": "a4806a3f-19ae-45c8-b739-cd47962e2c18",
        "title": "OSNR",
        "description": "Warehouse Activity Tracking System",
        "contentComponent": require('/src/components/IconImg').default,
      },
      {
        "id": "b8e5752a-dcea-4d22-b7aa-5db8a15064e1",
        "title": "SRS",
        "description": "Stocktake Reporting System",
        "contentComponent": require('/src/components/IconImg').default,
      }
    ]
  }
];
