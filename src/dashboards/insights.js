export default [
  {
    "section": "Insights",
    "tiles": [
      {
        "id": "fc3a1e0a-70b3-4d91-b1d0-56ee91559ed0",
        "title": "DGSC",
        "description": "DGSC Dynamic Reporting Tool",
        "contentComponent": require('/src/components/InisghtsGlance').default,
      },
      {
        "id": "a22eeb4c-8c3e-420a-8471-1b263b282b86",
        "title": "JLU Risks",
        "description": "JLU (SCB) Risk Dashboard",
        "contentComponent": require('/src/components/InisghtsGlance').default,,
        "link": "/dashboard/insights/jlu-risks"
      },
      {
        "id": "e05bf897-5ec7-4006-96e0-b8571123306a",
        "title": "Customer Support",
        "description": "Performance of JLUs in supporting customers",
        "contentComponent": require('/src/components/InisghtsGlance').default,
      },
      {
        "id": "e6efa572-02f8-4b1a-9d93-202c131e1719",
        "title": "W&D Contract",
        "description": "W&D Contract Performance Measures",
        "contentComponent": require('/src/components/InisghtsGlance').default,
      },
      {
        "id": "c7a89423-0383-4bb0-a507-8c28035c583c",
        "title": "BPM / BPT",
        "description": "Business Process Measurement & Testing",
        "contentComponent": require('/src/components/InisghtsGlance').default,
      },
      {
        "id": "1df1640d-e3f2-4fcf-a1b8-8a931d506169",
        "title": "SPM",
        "description": "Stocktake Performance",
        "contentComponent": require('/src/components/InisghtsGlance').default,
      }
    ]
  },
];
