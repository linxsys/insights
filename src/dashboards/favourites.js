import allSections from '/src/dashboard-data.js';

function customSection(sectionName, tileIds) {
  return [{
    section: sectionName,
    tiles: allSections.map(function (section) {
      const { tiles } = section;
      return tiles;
    }).flat().reduce((tiles, item) => {
      if (tileIds.includes(item.id)) {
        tiles.push(item);
      }
      return tiles;
    }, [])
  }]
}

export default customSection;