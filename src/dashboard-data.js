export default [
  {
    "section": "Apps",
    "tiles": [
      {
        "id": "9484dbf0-fa04-4f81-8646-5f16a6f58860",
        "title": "CFT",
        "description": "JLU Customer Feedback",
        "contentComponent": require('/src/components/IconImg').default,
        "contentProps": {
          "img": "cft_l.png"
        }
      },
      {
        "id": "cd0ae55e-dec2-4592-a77a-4f629f4ac1d6",
        "title": "ELMS",
        "description": "Loans Management",
        "contentComponent": require('/src/components/IconImg').default,
        "contentProps": {
          "img": "elms_l.png"
        }
      },
      {
        "id": "78596f8a-b50c-4281-991b-840768c28f77",
        "title": "WATS",
        "description": "Demand Cancellations",
        "contentComponent": require('/src/components/IconImg').default,
        "contentProps": {
          "img": "wats_l.png"
        }
      },
      {
        "id": "a4806a3f-19ae-45c8-b739-cd47962e2c18",
        "title": "OSNR",
        "description": "Ordered Service Notices",
        "contentComponent": require('/src/components/IconImg').default,
      },
      {
        "id": "b8e5752a-dcea-4d22-b7aa-5db8a15064e1",
        "title": "SRS",
        "description": "Stocktake Reporting",
        "contentComponent": require('/src/components/IconImg').default,
      }
    ]
  },
  {
    "section": "Insights",
    "tiles": [
      {
        "id": "fc3a1e0a-70b3-4d91-b1d0-56ee91559ed0",
        "title": "PuMP",
        "description": "SCB PuMP",
        "contentComponent": require('/src/components/IconMDI').default,
        "contentProps": {
          "icon": "mdi-chart-box-outline"
        },
        "link": "/dashboard/insights/pump"
      },
      {
        "id": "a22eeb4c-8c3e-420a-8471-1b263b282b86",
        "title": "JLU Risks",
        "description": "JLU (SCB) Risk Dashboard",
        "contentComponent": require('/src/components/IconMDI').default,
        "contentProps": {
          "icon": "mdi-chart-box-outline"
        },
        "link": "/dashboard/insights/jlu-risks"
      },
      {
        "id": "e05bf897-5ec7-4006-96e0-b8571123306a",
        "title": "Customer Support",
        "description": "Performance of JLUs",
        "contentComponent": require('/src/components/InsightsGlance').default,
      },
      {
        "id": "e6efa572-02f8-4b1a-9d93-202c131e1719",
        "title": "W&D Contract",
        "description": "W&D Contract KPIs",
        "contentComponent": require('/src/components/InsightsGlance').default,
      },
      {
        "id": "c7a89423-0383-4bb0-a507-8c28035c583c",
        "title": "BPM / BPT",
        "description": "Business Process Compliance",
        "contentComponent": require('/src/components/InsightsGlance').default,
      },
      {
        "id": "1df1640d-e3f2-4fcf-a1b8-8a931d506169",
        "title": "SPM",
        "description": "Stocktake Performance",
        "contentComponent": require('/src/components/InsightsGlance').default,
      }
    ]
  },
  {
    "section": "SCB JLU Risks",
    "tiles": [
      {
        "id": "d2b6b696-1861-4a31-8105-5c79324c1285",
        "title": "SSR",
        "description": "Servicable Stock",
        "contentComponent": require('/src/components/InsightsGlance').default,
      },
      {
        "id": "b66e39f1-5e02-46df-ba69-55a016249703",
        "title": "SLS",
        "description": "Shelf Life Stock",
        "contentComponent": require('/src/components/GlanceChartBullet').default,
      },
      {
        "id": "119da3db-5a4d-45eb-8af6-8ced47a8a5de",
        "title": "Loan Cache",
        "description": "Loan Cache Risk",
        "contentComponent": require('/src/components/GlanceChartParliament').default,
      },
      {
        "id": "aca3ba11-cb87-44d6-96f8-df5c20127721",
        "title": "IREQ",
        "description": "IREQ Re-activation Candidates",
        "contentComponent": require('/src/components/GlanceChartBar').default,
      }
    ]
  },
  {
    "section": "SCB PuMP",
    "tiles": [
      {
        "id": "d2b6b696-1861-4a31-8105-5c79324c1285",
        "title": "SSR",
        "description": "Servicable Stock",
        "contentComponent": require('/src/components/InsightsGlance').default,
      },
      {
        "id": "b66e39f1-5e02-46df-ba69-55a016249703",
        "title": "SLS",
        "description": "Shelf Life Stock",
        "contentComponent": require('/src/components/GlanceChartBullet').default,
      },
      {
        "id": "119da3db-5a4d-45eb-8af6-8ced47a8a5de",
        "title": "Loan Cache",
        "description": "Loan Cache Risk",
        "contentComponent": require('/src/components/GlanceChartParliament').default,
      },
      {
        "id": "aca3ba11-cb87-44d6-96f8-df5c20127721",
        "title": "IREQ",
        "description": "IREQ Re-activation Candidates",
        "contentComponent": require('/src/components/GlanceChartBar').default,
      }
    ]
  }
];
